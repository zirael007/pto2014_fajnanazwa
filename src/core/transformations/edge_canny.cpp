﻿#include "edge_canny.h"

#include "blur_gaussian.h"
#include "conversion_grayscale.h"
#include "edge_sobel.h"


EdgeCanny::EdgeCanny(PNM* img) :
    Convolution(img)
{
}

EdgeCanny::EdgeCanny(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

int getDir(int i) {
	if ((i >= 0 && i < 23) || (i >= 158 && i < 203) || (i >= 338 && i < 361))
	{
		return 0;
	}
	else if ((i >= 23 && i < 68) || (i >= 203 && i < 248))
	{
		return 1;
	}
	else if ((i >= 68 && i < 113) || (i >= 248 && i < 293))
	{
		return 2;
	}
	else if ((i >= 113 && i < 158) || (i >= 293 && i < 338))
	{
		return 3;
	}
	else return 3;
}


PNM* EdgeCanny::transform()
{
    int width  = image->width(),
        height = image->height();

    int upper_thresh = getParameter("upper_threshold").toInt(),
        lower_thresh = getParameter("lower_threshold").toInt();

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

	//Dokonaj konwersji obrazu wejściowego na skalę odcieni szarości.
	PNM* greyImage = ConversionGrayscale(image).transform();

	//Rozmyj otrzymany obraz filtrem Gaussa 3x3 z parametrem σ = 1.6
	BlurGaussian blurGauss(greyImage);
	blurGauss.setParameter("size", 3);
	blurGauss.setParameter("sigma", 1.6f);
	greyImage = blurGauss.transform();

	//Oblicz gradienty kierunkowe Gx i Gy operatorem Sobela przy pomocy metod rawHorizontalDetection() i rawVerticalDetection()
	EdgeSobel sobel(greyImage);
	math::matrix<float> *hGrad = sobel.rawHorizontalDetection();
	math::matrix<float> *vGrad = sobel.rawVerticalDetection();

	//Dla każdego piksela (i,j) oblicz moc oraz kierunek gradientu.

	math::matrix<float> orientation = math::matrix<float>(image->width(), image->height()); 
	math::matrix<float> magnitude = math::matrix<float>(image->width(), image->height());
				
	
	for (int x = 0; x < width; x++){
		for (int y = 0; y < height; y++){
			magnitude(x,y) = sqrt(pow((*hGrad)(x, y), 2) + pow((*vGrad)(x, y), 2));
			orientation(x, y) = (int)((atan2((*vGrad)(x, y), (*hGrad)(x, y)) / (M_PI)) * 180 + 360) / 360;
			newImage->setPixel(x, y, 0);
		}
	}
	
	
	//Przeprowadź tłumienie niemaksymalne, tj. dla każdego piksela (i,j)
	std::list<QPoint> init;
	for (int x = 1; x < width - 1; x++){
		for (int y = 1; y < height - 1; y++){
			int pixel1;
			int pixel2;
			float temp = orientation(x, y);
			int dir = getDir(temp);
			if (dir == 0) {
				pixel1 = magnitude((x - 1),y);
				pixel2 = magnitude((x + 1),y);
			}
		
		    else if (dir == 1) {
				pixel1 = magnitude((x - 1), (y - 1));
				pixel2 = magnitude((x + 1), (y + 1));
			}
			
			else if (dir == 2) {
				pixel1 = magnitude((x), (y - 1));
				pixel2 = magnitude((x), (y + 1));
			}
			else if (dir == 3) {
				pixel1 = magnitude((x + 1), (y + 1));
				pixel2 = magnitude((x - 1), (y - 1));
			}
			
			if ((magnitude(x,y) > pixel1) && (magnitude(x,y) > pixel2) && (magnitude(x,y) > upper_thresh)) {
					newImage->setPixel(x, y, 255);
					init.push_back(QPoint(x, y));
			}
			

		}
	}

	
	while (!init.empty())
	{
		QPoint p = init.front();
		init.pop_front();
		int pointOrient = orientation(p.x(), p.y());
	
		int x = p.x();
		int y = p.y();
		int direction = getDir(pointOrient);


		if (direction == 3)	{
			for (int i = x + 1; i<image->width(); i++)
				for (int j = y + 1; j<image->height(); j++)	{
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i + 1, j + 1) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
				}

	
			for (int i = x - 1; i>0; i--)
				for (int j = y - 1; j>0; j--){
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i - 1, j - 1) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
				}

		}

		if (direction == 2)	{
			int i = x;
			for (int j = y - 1; j>0; j--){
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i, j - 1) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
			}

			for (int j = y + 1; j<image->height(); j++)	{
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i, j + 1) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
			}
		}

		if (direction == 1)		{
			for (int i = x - 1; i>0; i--)
				for (int j = y + 1; j<image->height(); j++)	{
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i,j) != pointOrient) || (magnitude(i - 1, j + 1) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
				}

			for (int i = x + 1; i<image->width(); i++)
				for (int j = y - 1; j>0; j--)
				{
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i + 1, j - 1) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
				}

		}


		if (direction == 0)	{
			int j = y;
			for (int i = x - 1; i>0; i--){
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i - 1, j) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
			}

			for (int i = x + 1; i<image->width(); i++){
				if ((newImage->pixel(i, j) > 0) || (magnitude(i, j) < lower_thresh) || (orientation(i, j) != pointOrient) || (magnitude(i + 1, j) < magnitude(i, j)))
				{
					break;
				}
				newImage->setPixel(i, j, 255);
			}
		}


	}

	return newImage;
}

		


  