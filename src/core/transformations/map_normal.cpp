#include "map_normal.h"

#include "edge_sobel.h"
#include "map_height.h"

MapNormal::MapNormal(PNM* img) :
    Convolution(img)
{
}

MapNormal::MapNormal(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* MapNormal::transform()
{
    int width  = image->width(),
        height = image->height();

    double strength = getParameter("strength").toDouble();

	PNM* newImage = new PNM(width, height, image->format());

	PNM* tempImage = MapHeight(image).transform();
	EdgeSobel sobel(tempImage);
	math::matrix<float>* gx = sobel.rawHorizontalDetection();
	math::matrix<float>* gy = sobel.rawVerticalDetection();

	for (int x = 0; x<width; x++)
	{
		for (int y = 0; y<height; y++)
		{
			double dx = (*gx)(x, y) / 255;
			double dy = (*gy)(x, y) / 255;
			double dz = 1 / strength;
			double dlen = sqrt(pow(dx,2) + pow(dy,2) + pow(dz,2));
			double dxN = dx/dlen;
			double dyN = dy/dlen;
			double dzN = dz/dlen;
			double rNew = (dxN + 1.0) * (255 / strength);
			double gNew = (dyN + 1.0) * (255 / strength);
			double bNew = (dzN + 1.0) * (255 / strength);
			newImage->setPixel(x, y, qRgb(rNew, gNew, bNew));
		}
	}

	return newImage;
}
