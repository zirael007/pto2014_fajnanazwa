#include "edge_laplacian.h"

EdgeLaplacian::EdgeLaplacian(PNM* img) :
    Convolution(img)
{
}

EdgeLaplacian::EdgeLaplacian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

math::matrix<float> EdgeLaplacian::getMask(int, Mode)
{
	int size = getParameter("size").toInt();
	math::matrix<float> mask(size, size);
	int halfSize = size / 2;

	for (int x = 0; x < mask.rowno(); x++){
		for (int y = 0; y < mask.colno(); y++){
			if (x == halfSize && y == halfSize) mask(x, y) = size*size - 1;
			else mask(x, y) = -1;
		}
	}
	
	return mask;

}

