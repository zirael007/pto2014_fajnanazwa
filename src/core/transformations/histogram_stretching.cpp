#include "histogram_stretching.h"

#include "../histogram.h"

HistogramStretching::HistogramStretching(PNM* img) :
    Transformation(img)
{
}

HistogramStretching::HistogramStretching(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

int getRightEdge(QHash<int, int>* histogram) {
	int right = 0;
	for (QHash<int, int>::const_iterator i = histogram->begin(); i != histogram->end(); i++){
		if (i.value() > 0){
			if (i.key() > right)
				right = i.key();
		}
	}
	return right;
}

int getLeftEdge(QHash<int, int>* histogram) {
	int left = 255;
	for (QHash<int, int>::const_iterator i = histogram->begin(); i != histogram->end(); i++){
		if (i.value() > 0){
			if (i.key() < left)
				left = i.key();
			}
	}
	return left;
}




PNM* HistogramStretching::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

	if (image->format() == QImage::Format_RGB32) {
		QHash<int, int>* channelR = image->getHistogram()->get(Histogram::RChannel);
		QHash<int, int>* channelG = image->getHistogram()->get(Histogram::GChannel);
		QHash<int, int>* channelB = image->getHistogram()->get(Histogram::BChannel);
		int minR = getLeftEdge(channelR);
		int maxR = getRightEdge(channelR);
		int minG = getLeftEdge(channelG);
		int maxG = getRightEdge(channelG);
		int minB = getLeftEdge(channelB);
		int maxB = getRightEdge(channelB);

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				QRgb pixel = image->pixel(x, y);
				int r = qRed(pixel);    // Get the 0-255 value of the R channel
				int g = qGreen(pixel);  // Get the 0-255 value of the G channel
				int b = qBlue(pixel);   // Get the 0-255 value of the B channel
				float tempR = (float)255 / (maxR - minR);
				float tempG = (float)255 / (maxG - minG);
				float tempB = (float)255 / (maxB - minB);
				r = (tempR * (r - minR));
				g = (tempG * (g - minG));
				b = (tempB * (b - minB));

				if (r > 255) r = 255;
				if (g > 255) g = 255;
				if (b > 255) b = 255;
				if (r < 0)  r = 0;
				if (g < 0)  g = 0;
				if (b < 0)  b = 0;


				QColor newPixel = QColor(r, g, b);
				newImage->setPixel(x, y, newPixel.rgb());

			}
		}
	
	}
	else {

		QHash<int, int>* channelL = image->getHistogram()->get(Histogram::LChannel);
		int minL = getLeftEdge(channelL);
		int maxL = getRightEdge(channelL);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int gray = qGray(image->pixel(x, y));
				float tempL = (float)255 / (maxL - minL);
				gray = (tempL * (gray - minL));
				if (gray > 255) gray = 255;
				if (gray < 0)  gray = 0;
				newImage->setPixel(x, y, gray);
			}
		}
	}
	
    return newImage;
}
