#include "histogram_equalization.h"

#include "../histogram.h"

HistogramEqualization::HistogramEqualization(PNM* img) :
    Transformation(img)
{
}

HistogramEqualization::HistogramEqualization(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* HistogramEqualization::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

	double factor = 255.0 / (width*height);

	if (image->format() == QImage::Format_Indexed8)
	{
		QHash<int, int>* histL = image->getHistogram()->get(Histogram::LChannel);

		int distribL[256];
		int probL[256];
		int allPixels = height*width;
		int partialSum = 0;
		
		//prawdopodobie�stwo
		for (int i = 0; i <= 255; i++)
		{
			probL[i] = (histL->value(i, 0));
		}

		//dystrybuanta

		for (int i = 0; i<256; i++){
			partialSum = partialSum + probL[i];
			distribL[i] = partialSum;
		}


		for (int x = 0; x<width; x++)
			for (int y = 0; y<height; y++)
			{
			int color = qGray(image->pixel(x, y)); 
			int newColor = (int)(distribL[color] * 255/allPixels);
			if (newColor < 0) newColor = 0;
			if (newColor > 255) newColor = 255;
			newImage->setPixel(x, y, newColor);
			}
	}
	
	else 
	{
		QHash<int, int>* histR = image->getHistogram()->get(Histogram::RChannel);
		QHash<int, int>* histG = image->getHistogram()->get(Histogram::GChannel);
		QHash<int, int>* histB = image->getHistogram()->get(Histogram::BChannel);

		int distribR[256];
		int distribG[256];
		int distribB[256];
		int probR[256];
		int probG[256];
		int probB[256];
		int allPixels = height*width;
		int partialSum = 0;

		//prawdopodobie�stwo
		for (int i = 0; i <= 255; i++)
		{
			probR[i] = histR->value(i, 0);
			probG[i] = histG->value(i, 0);
			probB[i] = histB->value(i, 0);
		}

		for (int i = 0; i<256; i++){
			partialSum = partialSum + probR[i];
			distribR[i] = partialSum;
		}

		partialSum = 0;

		for (int i = 0; i<256; i++){
			partialSum = partialSum + probG[i];
			distribG[i] = partialSum;
		}

		partialSum = 0;

		for (int i = 0; i<256; i++){
			partialSum = partialSum + probB[i];
			distribB[i] = partialSum;
		}

		for (int x = 0; x<width; x++)
			for (int y = 0; y<height; y++)
			{
			QRgb color = image->pixel(x, y);
			int r = qRed(color);
			int g = qGreen(color);
			int b = qBlue(color);

			r = (int)(distribR[r] * 255 / allPixels);
			g = (int)(distribG[g] * 255 / allPixels);
			b = (int)(distribB[b] * 255 / allPixels);

			newImage->setPixel(x, y, qRgb(r,g,b));
			}
	}

	return newImage;
}


