#include "edge_sobel.h"

EdgeSobel::EdgeSobel(PNM* img, ImageViewer* iv) :
    EdgeGradient(img, iv)
{
    prepareMatrices();
}

EdgeSobel::EdgeSobel(PNM* img) :
    EdgeGradient(img)
{
    prepareMatrices();
}

void EdgeSobel::prepareMatrices()
{
	math::matrix<float> x(3, 3);
	math::matrix<float> y(3, 3);

	x(0, 0) = -1;
	x(0, 1) = 0;
	x(0, 2) = 1;
	x(1, 0) = -2;
	x(1, 1) = 0;
	x(1, 2) = 2;
	x(2, 0) = -1;
	x(2, 1) = 0;
	x(2, 2) = 1;
	g_x = x;

	y(0, 0) = -1;
	y(0, 1) = -2;
	y(0, 2) = -1;
	y(1, 0) = 0;
	y(1, 1) = 0;
	y(1, 2) = 0;
	y(2, 0) = 1;
	y(2, 1) = 2;
	y(2, 2) = 1;

	g_y = y;
}

math::matrix<float>* EdgeSobel::rawHorizontalDetection()
{
    math::matrix<float>* x_gradient = new math::matrix<float>(this->image->width(), this->image->height());

	int width = x_gradient->rowno();
	int height = x_gradient->colno();

	for (int x = 0; x<width; x++)
		for (int y = 0; y<height; y++)
		{
			math::matrix<float> window = getWindow(x, y, 3, LChannel, NullEdge);
			(*x_gradient)(x, y) = sum(join(g_x, window));
		}

	return x_gradient;
}

math::matrix<float>* EdgeSobel::rawVerticalDetection()
{
    math::matrix<float>* y_gradient = new  math::matrix<float>(this->image->width(), this->image->height());

	int width = y_gradient->rowno();
	int height = y_gradient->colno();

	for (int x = 0; x<width; x++)
		for (int y = 0; y<height; y++)
		{
			math::matrix<float> window = getWindow(x, y, 3, LChannel, NullEdge);
			(*y_gradient)(x, y) = sum(join(g_y, window));
		}

	return y_gradient;
}
