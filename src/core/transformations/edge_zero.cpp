#include "edge_zero.h"

#include "edge_laplacian_of_gauss.h"

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img) :
    Convolution(img)
{
}

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

float findMin(math::matrix<float> matrix, int size) {
	float min = 255;
	for (int x = 0; x < size; x++) {
		for (int y = 0; y < size; y++) {
			if (matrix(x, y)<min) {
				min = matrix(x, y);
			}
		}
	}
	return min;
}

float findMax(math::matrix<float> matrix, int size) {
	float max = 0;
	for (int x = 0; x < size; x++) {
		for (int y = 0; y < size; y++) {
			if (matrix(x, y)>max) {
				max = matrix(x, y);
			}
		}
	}
	return max;
}


PNM* EdgeZeroCrossing::transform()
{
    int width = image->width(),
        height = image->height();

    int    size  = getParameter("size").toInt();
    double sigma = getParameter("sigma").toDouble();
    int    t     = getParameter("threshold").toInt();

   	EdgeLaplaceOfGauss laplaceOfGauss(image);
	laplaceOfGauss.setParameter("size", size);
	laplaceOfGauss.setParameter("sigma", sigma);

	PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

	Transformation laplace(laplaceOfGauss.transform());
	int half = size / 2;


	int v0 = 128;

	if (image->format() == QImage::Format_Indexed8) {
		float maximum;
		float minimum;
		for (int x = 0; x<width; x++) {
			for (int y = 0; y<height; y++) {
				math::matrix<float> maska = laplace.getWindow(x, y, size, LChannel, NullEdge);
				maximum = findMax(maska, size);
				minimum = findMin(maska, size);
				int q;
				if ((minimum < (v0 - t)) && (maximum >(v0 + t))) q = maska(half, half);						
				else q = 0;
				newImage->setPixel(x, y, q);				
			}
		}
	}

	else {
		float maximum;
		float minimum;
		for (int x = 0; x<width; x++) {
			for (int y = 0; y < height; y++) {
				math::matrix<float> windowR = laplace.getWindow(x, y, size, RChannel, NullEdge);
				math::matrix<float> windowG = laplace.getWindow(x, y, size, GChannel, NullEdge);
				math::matrix<float> windowB = laplace.getWindow(x, y, size, BChannel, NullEdge);
				float maxR = findMax(windowR, size);
				float maxG = findMax(windowG, size);
				float maxB = findMax(windowB, size);
				float minR = findMin(windowR, size);
				float minG = findMin(windowG, size);
				float minB = findMin(windowB, size);
				
				if ((minR < (v0 - t)) && (maxR >(v0 + t))) newImage->setPixel(x, y, windowR(half, half));
			    if ((minG < (v0 - t)) && (maxG >(v0 + t))) newImage->setPixel(x, y, windowG(half, half));
				if ((minB < (v0 - t)) && (maxB >(v0 + t))) newImage->setPixel(x, y, windowB(half, half));
				else  newImage->setPixel(x, y, 0);

			}
		}
	}


    return newImage;
}

