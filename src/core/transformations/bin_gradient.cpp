#include "bin_gradient.h"

BinarizationGradient::BinarizationGradient(PNM* img) :
    Transformation(img)
{
}

BinarizationGradient::BinarizationGradient(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationGradient::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

	int dividend = 0;
	int divisor = 0;
	int gx = 0;
	int gy = 0;
	int threshold = 0;

	for (int x = 0; x<width; x++)
		for (int y = 0; y<height; y++)
		{
			gx = qGray(image->pixel(x + 1, y)) - qGray(image->pixel(x - 1, y));
			gy = qGray(image->pixel(x, y + 1)) - qGray(image->pixel(x, y - 1));
			if (gx >= gy) gy = gx;
			QRgb pixel = image->pixel(x, y);
			dividend = dividend + (qGray(pixel) * gy);
			divisor = divisor + gy;
			threshold = dividend / divisor;
			
			if (qGray(pixel) >= threshold) newImage->setPixel(x, y, Qt::color1);
			else newImage->setPixel(x, y, Qt::color0);
		}

    return newImage;
}


