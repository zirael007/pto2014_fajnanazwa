#include "noise_median.h"

NoiseMedian::NoiseMedian(PNM* img) :
    Convolution(img)
{
}

NoiseMedian::NoiseMedian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseMedian::transform()
{
    int width  = image->width();
    int height = image->height();

 	PNM* newImage = new PNM(width, height, image->format());

	int median = 0;
	int medianG = 0;
	int medianB = 0;
	
	if (image->format() == QImage::Format_Indexed8){
		for (int i = 0; i<width; i++){
			for (int j = 0; j<height; j++){
				median = getMedian(i, j, LChannel);
				newImage->setPixel(i, j, median);
			}
		}
	}
	else {
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				median = getMedian(i, j, RChannel);
				medianG = getMedian(i, j, GChannel);
				medianB = getMedian(i, j, BChannel);
				newImage->setPixel(i, j, qRgb(median, medianG, medianB));
			}
		}
	}

    return newImage;
}

int NoiseMedian::getMedian(int x, int y, Channel channel)
{
    int radius = getParameter("radius").toInt();
	math::matrix<float> window = getWindow(x, y, 2 * radius + 1, channel, RepeatEdge);

	std::vector<int> wektor;
	std::vector<int>::iterator it;

	it = wektor.begin();
	for (int i = 0; i < window.rowno(); i++) 
	{
		for (int j = 0; j < window.colno(); j++) 
				it = wektor.insert(it, window(i, j));
	}

	std::sort(wektor.begin(), wektor.end());

	return wektor.at(wektor.size() / 2);
}
