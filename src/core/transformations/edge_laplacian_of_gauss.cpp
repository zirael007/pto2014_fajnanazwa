#include "edge_laplacian_of_gauss.h"

#include "blur_gaussian.h"

EdgeLaplaceOfGauss::EdgeLaplaceOfGauss(PNM* img) :
    Convolution(img)
{
}

EdgeLaplaceOfGauss::EdgeLaplaceOfGauss(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

math::matrix<float> EdgeLaplaceOfGauss::getMask(int, Mode)
{
    size = getParameter("size").toInt();
    double sigma = getParameter("sigma").toDouble();
	int half = size / 2;

    math::matrix<float> mask(size, size);

	for (int x = 0; x<size; x++)
	{
		for (int y = 0; y<size; y++)
		{
			mask(x, y) = getLoG(x - half, y - half, sigma);
		}
	}

    return mask;
}

float EdgeLaplaceOfGauss::getLoG(int x, int y, float s)
{
	float factor1 = (((x*x + y*y) / s*s - 2) / s*s);
	float factor2 = BlurGaussian::getGauss(x, y, s);
	float product = factor1*factor2;
	
	return product;
}

int EdgeLaplaceOfGauss::getSize()
{
    return size;
}
