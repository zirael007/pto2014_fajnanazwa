#include "noise_bilateral.h"
const float EulerConstant = std::exp(1.0);

NoiseBilateral::NoiseBilateral(PNM* img) :
    Convolution(img)
{
}

NoiseBilateral::NoiseBilateral(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseBilateral::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    sigma_d = getParameter("sigma_d").toInt();
    sigma_r = getParameter("sigma_r").toInt();
    radius = sigma_d;
	
	int red = 0;
	int green = 0;
	int blue = 0;

	if (image->format() == QImage::Format_Indexed8){
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				newImage->setPixel(i, j, calcVal(i, j, Channel::LChannel));
			}
		}
	}
	else if (image->format() == QImage::Format_RGB32){
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				red = calcVal(i, j, Channel::RChannel);
				green = calcVal(i, j, Channel::GChannel);
				blue = calcVal(i, j, Channel::BChannel);
				QColor newValue = QColor(red, green, blue);
				newImage->setPixel(i, j, newValue.rgb());
			}
		}
	}

    

    return newImage;
}

int NoiseBilateral::calcVal(int x, int y, Channel channel)
{
	int pixel = image->pixel(x, y);
	int pixelVal = 0;

	math::matrix<float> window = getWindow(x, y, radius * 2 + 1, channel, Transformation::RepeatEdge);
	switch (channel) {
		case(LChannel) : pixelVal = qGray(pixel);
			break;
		case(RChannel) : pixelVal = qRed(pixel);
			break;
		case(GChannel) : pixelVal = qGreen(pixel);
			break;
		case(BChannel) : pixelVal = qBlue(pixel);
			break;
	}

	     
	float numerator = 0;
	float denominator = 0;
	float belowMultiplication = 0;
	for (int i = 0; i < window.rowno(); i++)
	{
		for (int j = 0; j < window.colno(); j++)
		{
			belowMultiplication = colorCloseness((int)window(i, j), pixelVal) * spatialCloseness(QPoint(x - radius, y - radius), QPoint(x, y)) ;
			numerator += (window(i, j) * belowMultiplication);
			denominator += belowMultiplication;
		}
	}

	
	if (denominator != 0){
		return (int)(numerator / denominator);
	}

	else return 0;
}

float NoiseBilateral::colorCloseness(int val1, int val2)
{
	float temp = pow(val1 - val2, 2.0) / (2 * pow(sigma_r, 2.0));
	return pow(EulerConstant, -temp);
}

float NoiseBilateral::spatialCloseness(QPoint point1, QPoint point2)
{
	return pow(EulerConstant, -((pow(point1.x() - point2.x(), 2.0) + pow(point1.y() - point2.y(), 2.0)) / (2 * pow(sigma_d, 2.0))));
}
