#include "convolution.h"

/** Overloaded constructor */
Convolution::Convolution(PNM* img) :
Transformation(img)
{
}

Convolution::Convolution(PNM* img, ImageViewer* iv) :
Transformation(img, iv)
{
}

/** Returns a convoluted form of the image */
PNM* Convolution::transform()
{
	return convolute(getMask(3, Normalize), RepeatEdge);
}

/** Returns a sizeXsize matrix with the center point equal 1.0 */
math::matrix<float> Convolution::getMask(int size, Mode mode = Normalize)
{
	math::matrix<float> mask(size, size);

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			mask(i, j) = 0.0;
			if (i == (int)(size / 2) && j == (int)(size / 2)) {
				mask(i, j) = 1.0;
			}
		}
	}

	return mask;
}

/** Does the convolution process for all pixels using the given mask. */
PNM* Convolution::convolute(math::matrix<float> mask, Mode mode = RepeatEdge)
{
	int width = image->width(),
		height = image->height();

	PNM* newImage = new PNM(width, height, image->format());
	float weight = sum(mask); 

	if (image->format() == QImage::Format_Indexed8)
	{
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				math::matrix<float> window = getWindow(x, y, mask.rowno(), LChannel, mode);
				math::matrix<float> accumulator = join(window, reflection(mask));
				float accumulatorSum = sum(accumulator);
				if (weight != 0) accumulatorSum /= weight;
				accumulatorSum > 255 ? accumulatorSum = 255 : (accumulatorSum < 0 ? accumulatorSum = 0 : accumulatorSum);
			    newImage->setPixel(x, y, (int)accumulatorSum);
			}
		}
	}
	else {	
		for (int x = 0; x < width; x++)	{
			for (int y = 0; y < height; y++) {
				math::matrix<float> windowR = getWindow(x, y, mask.rowno(), RChannel, mode);
				math::matrix<float> windowG = getWindow(x, y, mask.rowno(), GChannel, mode);
				math::matrix<float> windowB = getWindow(x, y, mask.rowno(), BChannel, mode);
				math::matrix<float> accumulatorR = join(windowR, reflection(mask));
				math::matrix<float> accumulatorG = join(windowG, reflection(mask));
				math::matrix<float> accumulatorB = join(windowB, reflection(mask));
				
				float sumR = sum(accumulatorR);
				float sumG = sum(accumulatorG);
				float sumB = sum(accumulatorB);

				if (weight != 0) {
					sumR /= weight;
					sumG /= weight;
					sumB /= weight;
				}

				sumR > 255 ? sumR = 255 : (sumR < 0 ? sumR = 0 : sumR);
				sumG > 255 ? sumG = 255 : (sumG < 0 ? sumG = 0 : sumG);
				sumB > 255 ? sumB = 255 : (sumB < 0 ? sumB = 0 : sumB);

				QColor new_value = QColor((int)(sumR), (int)(sumG), (int)(sumB));
				newImage->setPixel(x, y, new_value.rgb());
			}
		}
	}

	return newImage;
}

/** Joins to matrices by multiplying the A[i,j] with B[i,j].
* Warning! Both Matrices must be squares with the same size!
*/
const math::matrix<float> Convolution::join(math::matrix<float> A, math::matrix<float> B)
{
	int size = A.rowno();
	math::matrix<float> C(size, size);

	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			C(i, j) = A(i, j)*B(i, j);
		}
	}

	return C;
}

/** Sums all of the matrixes elements */
const float Convolution::sum(const math::matrix<float> A)
{
	int size = A.rowno();
	float sum = 0.0;

	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			sum = sum + A(i, j);
		}
	}

	return sum;

}


/** Returns reflected version of a matrix */
const math::matrix<float> Convolution::reflection(const math::matrix<float> A)
{
	int size = A.rowno();
	math::matrix<float> C(size, size);

	for (int i = 0; i < size; i++){
		for (int j = 0; j < size; j++){
			C(i, j) = A((size - 1 - i), (size - 1 - j));
		}
	}

	return C;
}