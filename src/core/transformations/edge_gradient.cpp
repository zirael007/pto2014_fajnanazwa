#include "edge_gradient.h"

EdgeGradient::EdgeGradient(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

EdgeGradient::EdgeGradient(PNM* img) :
    Convolution(img)
{
}

PNM* EdgeGradient::verticalDetection()
{
    return convolute(g_y, RepeatEdge);
}

PNM* EdgeGradient::horizontalDetection()
{
    return convolute(g_x, RepeatEdge);
}

PNM* EdgeGradient::transform()
{
    PNM* newImage = new PNM(image->width(), image->height(), image->format());

	int width = image->width();
	int height = image->height();

	PNM* xImage = horizontalDetection();
	PNM* yImage = verticalDetection();

	if (image->format() == QImage::Format_RGB32)
	{
		for (int i = 0; i<width; i++)
			for (int j = 0; j<height; j++)
			{
			QRgb x = xImage->pixel(i, j);
			QRgb y = yImage->pixel(i, j);
			int xB = qBlue(x);
			int yB = qBlue(y);
			int xR = qRed(x);
			int yR = qRed(y);
			int xG = qGreen(x);
			int yG = qGreen(y);
			int newR = sqrt(xR*xR + yR*yR);
			int newG = sqrt(xG*xG + yG*yG);
			int newB = sqrt(xB*xB + yB*yB);
			if (newR<0) {
				newR = 0;
			}
			else if (newR>255) {
				newR = 255;
			}
			if (newG<0) {
				newG = 0;
			}
			else if (newG>255) {
				newG = 255;
			}
			if (newB<0) {
				newB = 0;
			}
			else if (newB>255) {
				newB = 255;
			}
			QColor new_value = QColor((int)(newR), (int)(newG), (int)(newB));

			newImage->setPixel(i, j, new_value.rgb());

			}
	}

	else {
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
			{
			QRgb x = xImage->pixel(i, j);
			QRgb y = yImage->pixel(i, j);
			int xG = qGray(x);
			int yG = qGray(y);
			int l = (int)sqrt(xG*xG + yG*yG);

			newImage->setPixel(i, j, l);
			}
	}
    return newImage;
}

