#include "corner_harris.h"

#include "blur_gaussian.h"
#include "conversion_grayscale.h"
#include "edge_sobel.h"

CornerHarris::CornerHarris(PNM* img) :
Convolution(img)
{
}

CornerHarris::CornerHarris(PNM* img, ImageViewer* iv) :
Convolution(img, iv)
{
}

PNM* CornerHarris::transform()
{
	int    threshold = getParameter("threshold").toInt();
	double sigma = getParameter("sigma").toDouble(),
		sigma_weight = getParameter("sigma_weight").toDouble(),
		k_param = getParameter("k").toDouble();

	int width = image->width(),
		height = image->height();

	PNM* newImage = new PNM(width, height, QImage::Format_Mono);


	//macierze zerowe
	math::matrix<float> I_xx(width, height);
	math::matrix<float> I_yy(width, height);
	math::matrix<float> I_xy(width, height);
	math::matrix<float> corner_candidates(width, height);
	math::matrix<float> corners_nonmax_suppress(width, height);
	math::matrix<float> H(2, 2);

	//konwersja obrazu do skali szaro�ci
	PNM* greyImage = ConversionGrayscale(image).transform();

	//rozmycie filtrem Gaussa
	BlurGaussian blurred(greyImage);
	blurred.setParameter("size", 3);
	blurred.setParameter("sigma", 3.6);
	greyImage = blurred.transform();

	//obliczanie gradient�w kierunkowych
	EdgeSobel sobel(greyImage);
	math::matrix<float>* g_x = sobel.rawHorizontalDetection();
	math::matrix<float>* g_y = sobel.rawVerticalDetection();


	//dla ka�dego pixela
	for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++){
			I_xx(i, j) = pow((*g_x)(i, j), 2);
			I_yy(i, j) = pow((*g_y)(i, j), 2);
			I_xy(i, j) = (*g_x)(i, j) * (*g_y)(i, j);
		}
	


	for (int i = 0; i < width; i++){
		for (int j = 0; j < height; j++){
			//je�li le�� na brzegu obrazu, to = 0
			if (i == 0 || j == 0 || i == width - 1 || j == height - 1){
				corner_candidates(i, j) = 0;
				corners_nonmax_suppress(i, j) = 0;
			}

			//je�eli piksel nie le�y na brzegu obrazu
			else {
				float S_xx = 0, S_yy = 0, S_xy = 0;
				for (int k = -1; k < 2; k++)
					for (int l = -1; l < 2; l++){
						S_xx += (I_xx(i + k, j + l)*BlurGaussian::getGauss(k, l, sigma))/sigma_weight;
						S_yy += (I_yy(i + k, j + l)*BlurGaussian::getGauss(k, l, sigma))/sigma_weight;
						S_xy += (I_xy(i + k, j + l)*BlurGaussian::getGauss(k, l, sigma))/sigma_weight;
					}
				
	
				
				H(0, 0) = S_xx; 
				H(0, 1) = S_xy;				
				H(1, 0) = S_xy;
				H(1, 1) = S_yy;

				float r = H(0, 0)*H(1, 1) - H(0, 1)*H(1, 0) - k_param*((H(0, 0) + H(1, 1))*(H(0, 0) + H(1, 1)));
				if (r > threshold) corner_candidates(i, j) = r;
				else  corner_candidates(i, j) = 0;
				//corners_candidates(i, j) = (r > threshold) ? r : 0;
			}
		}
	}

	//punkt 8
	bool search = true;
	while (search){
		search = false;
		//dla ka�dego piksela, kt�ry nie le�y na brzegu
		for (int i = 1; i < width - 1; i++)
			for (int j = 1; j < height - 1; j++){
				double val = corner_candidates(i, j);
				if (corner_candidates(i, j) > corner_candidates(i - 1, j - 1) &&
					corner_candidates(i, j) > corner_candidates(i - 1, j + 1) &&
					corner_candidates(i, j) > corner_candidates(i, j - 1) &&
					corner_candidates(i, j) > corner_candidates(i, j + 1) &&
					corner_candidates(i, j) > corner_candidates(i - 1, j) &&
					corner_candidates(i, j) > corner_candidates(i + 1, j) &&
					corner_candidates(i, j) > corner_candidates(i + 1, j - 1) &&
					corner_candidates(i, j) > corner_candidates(i + 1, j + 1)){
					corners_nonmax_suppress(i, j) = val;
				}
				else {
					if (corner_candidates(i, j) > 0)
						search = true;
					corners_nonmax_suppress(i, j) = 0;
				}
			}
		
		corner_candidates = corners_nonmax_suppress;
	}


	//punkt 9
	for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++){
			if (corner_candidates(i, j) == 0)
				newImage->setPixel(i, j, 0);
			else
				newImage->setPixel(i, j, 1);
		}
	

	return newImage;
}