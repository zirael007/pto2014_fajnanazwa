#include "hough.h"

#include "conversion_grayscale.h"
#include "edge_laplacian.h"
#include "edge_prewitt.h"

Hough::Hough(PNM* img) :
    Transformation(img)
{
}

Hough::Hough(PNM* img, ImageViewer* super) :
    Transformation(img, super)
{
}

PNM* Hough::transform()
{   
	int thetaDensity = getParameter("theta_density").toInt();
	int width = image->width(),
		height = image->height();

	//konwersja na skal� szarosci
	PNM* greyImage = ConversionGrayscale(image).transform();

	//wykrywanie krawedzi
	if (getParameter("skip_edge_detection").toBool() == false){
		EdgePrewitt prewitt(greyImage);
		prewitt.setParameter("size", 2);
		greyImage = prewitt.transform();
	}

	//przekatna
	int diagonal = sqrt(pow(width, 2) + pow(height, 2));
    int thetaSize = thetaDensity * 180;

	//tworzenie obrazu wyjsciowego
	PNM* newImage = new PNM(thetaSize, diagonal * 2 + 1, QImage::Format_Indexed8);
	math::matrix<int> hough(thetaSize,diagonal * 2 + 1);
	
	//tworzenie macierzy zerowej
	for (int x = 0; x < thetaSize - 1; x++){
		for (int y = 0; y < diagonal * 2; y++){
			hough(x, y) = 0;
		}
	}

	//punkt 7 zadania
	double theta = 0;
	int rho = 0;
	for (int i = 0; i < width; i++){
		for (int j = 0; j < height; j++){
			int v = qBlue(greyImage->pixel(i, j));
			if (v > 0){
				for (int k = 0; k < thetaSize; k++){
					theta = (k*M_PI) / (180.0*thetaDensity);
					rho = round(i*cos(theta) + j*sin(theta));
					hough(k, rho + diagonal)++;
				}
			}
		}
	}

	//znalezienie maksimum macierzy do normalizacji
	int maximum = 0;
	for (int x = 0; x < thetaSize - 1; x++)
		for (int y = 0; y < diagonal * 2; y++){
			if (hough(x, y) > maximum)
				maximum = hough(x, y);
	}

	//wypelnienie obrazu wyjsciowego znormalizowanymi wartosciami
	int color;
	for (int i = 0; i < newImage->width(); i++)
		for (int j = 0; j < newImage->height(); j++){
			color = round(255 * hough(i, j) / maximum);
			newImage->setPixel(i, j, color);
		
	}

	return newImage;
}
