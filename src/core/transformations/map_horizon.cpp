#include "map_horizon.h"

#include "map_height.h"

MapHorizon::MapHorizon(PNM* img) :
    Transformation(img)
{
}

MapHorizon::MapHorizon(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* MapHorizon::transform()
{
    int width  = image->width(),
        height = image->height();

    double scale     = getParameter("scale").toDouble();
    int    sun_alpha = getParameter("alpha").toInt();
    int dx, dy;

    switch (getParameter("direction").toInt())
    {
    case NORTH: dx = 0; dy = -1; break;
    case SOUTH: dx = 0; dy = 1; break;
    case EAST:  dx = 1; dy = 0; break;
    case WEST:  dx = -1; dy = 0; break;
    default:
        qWarning() << "Unknown direction!";
        dx =  0;
        dy = -1;
    }

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);


	PNM* mapHeight = MapHeight(image).transform();
	for (int x = 0; x<width; x++)
		for (int y = 0; y < height; y++)
		{
		double alpha = 0;
		int currentH = qGray(mapHeight->pixel(x, y));
		for (int k = x + dx, l = y + dy; k < width && k >= 0 && l < height && l >= 0; k += dx, l += dy)
		{
			int rayH = qGray(mapHeight->pixel(k, l));
			if (currentH < rayH)
			{
				double dist = sqrt((k - x)*(k - x) + (l - y)*(l - y)) * scale;
				double rayA = atan((rayH - currentH) / dist);
				if (rayA > alpha)  alpha = rayA;
			}
		}
		double delta = alpha - sun_alpha * M_PI / 180;
		if (delta > 0) newImage->setPixel(x, y, cos(delta) * 255);
		else newImage->setPixel(x, y, 255);
		}
	return newImage;
}
